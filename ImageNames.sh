#! /bin/bash
# Script:    ImageNames.sh
# Purpose:   Transfer image and video files any (?) digital camera or memory card
#            and rename them according to the date taken.
# Author:    Guillaume Dargaud - http://www.gdargaud.net/
# Copyright: (c) 2006 - Free use, distribution and modification
# System:    This works on Linux and Windows+Cygwin
# Cameras:   Works will all my cameras and can be customized for any type of files
#            as long as they contain date information
# File types jpeg, raf, raw, tif, mp3, wav, avi... See the iregex variable
#
# Workflow:
#   The first time, put this script in an empty directory and start it from there.
#   If connecting the camera, set the camera to [Usb Mass Storage] mode and connect it.
#	If using a card reader, plug the card in.
#	Change the 'PathCamera' line below accordingly.
#   Run the present script from a bash window and cd into the directory
#	(otherwise it will copy to files to the current directory)
#
#   It will move the files from the camera to the present directory
#   and then move and rename them to YYYYMMDD\YYYYMMDD-HHMMSS.ext
#   You can then rename the directories and files to something more meaningful
#   For instance I use YYYYMMDD_PlaceOrEventName\YYYYMMDD-HHMMSS_SomeKeywords.ext
#   Next time you run it, new files are added, the effect is cummulative.
#
# Make sure you have a backup of the whole directory the first few times you run this
# I take no responsability if you wipe out 2000 images or your camera catches fire.
#
# Known bug: - sometimes on Windows/cygwin there are files remaining in the directory after a run,
#              simply run the script a second time.
#            - sometimes (!) on Windows/cygwin some files are moved to C:
#            - some file type (.mov) do not contain date info, so the script doesn't rename them
#
# You need cygwin to run this on Windows: http://www.cygwin.com/


# This is the directory where the camera files are found
# when you connect the USB cable OR use a card reader (faster)
# ---> MAKE SURE THIS IS CORRECT or files won't be found <---

## This is specific to a given camera on Windows
#PathCamera=$( cygpath "E:\DCIM\100RICOH" )

# The following works with multiple cards inserted at the same time
# This looks for any kind of card reader or USB-mounted camera drive on Windows (recommanded)
#PathCamera=/cygdrive/*/DCIM/*

# This looks on linux mounted directories (recommanded)
#PathCamera=/media/*/DCIM/*
# Optional. One of my cameras places its videos there (mts files). Comment it if not necessary
#PathSecondary=/media/*/PRIVATE/AVCHD/

# Same as above, but this is for new ubuntu 13.10 user-dependant mount-points
PathCamera=/media/$USER/*/DCIM/*
PathSecondary=/media/$USER/*/PRIVATE/AVCHD/

# Specifics files extensions to deal with (case insensitive).
# Add here as needed with \| separators
# The classification is not very important
Raws="rw2\|raf\|raw\|dng\|nef\|ndf"
Work="tif\|png"
Final="jpg\|jpeg"
Movies="avi\|mp4\|mpg\|mpeg\|mov\|3gp\|mts\|webm"
Sounds="wav\|mp3\|au\|mob"

# Let's merge them all
Pics="$Raws\|$Work\|$Final"			# All wanted pictures
iregex="$Pics\|$Movies\|$Sounds"	# All wanted files

# Files that don't contain date info are renamed according to the file date on a 2nd pass (less reliable)
# Put file extensions here only if they don't contain dates,
# test it with: strings FILENAME | grep 2014 (or year the image was taken)
others="mov\|3gp\|mts"
# Or you can just default to whatever failed on first pass if you have files leftover in you directory:
# others=$iregex

# Lets operate in 'current year' directory, even if some files may be older
YEAR=$(date +%Y)
mkdir -p ~/Pictures/$YEAR
cd ~/Pictures/$YEAR

# You shouldn't have anything to change below here


###############################################################################
# Generate a filename that doesn't already exist
# Name passed should be .*[0-9].ext
# Note that if there are pairs of files with same times: a.raw, a.jpg, b.raw, b.jpg
#      they may end up mismatched
###############################################################################
function GetUniqueName {
  if [ $# -ne 1 ]; then exit; fi
  File=$1
  while [ -e "$File" ]
  do
	echo "EXIST: $File" >&2
	# If there is no letter before the extension, add the original file number
	# If there is a letter before the extension, increase it
	if [[ $File =~ .*[0-9]\.[^.]* ]]
	then
	  File=$( echo "$File" | sed -e "s/\(.*[0-9]\)\(\.[^.]*\)/\1a\2/" )
	elif [[ $File =~ .*[0-9][a-z]\.[^.]* ]]
	then
	  Letter=$( echo "$File" | sed -e "s/.*[0-9]\([a-z]\)\.[^.]*/\1/" | tr "abcdefghijklmnopqrstuvwxyz" "bcdefghijklmnopqrstuvwxyz_" )
	  File=$( echo "$File" | sed -e "s/\(.*[0-9]\)\([a-z]\)\(\.[^.]*\)/\1$Letter\3/" )
	else
	  echo "Wrong filename format: $File" >&2
	  echo "$1"	# Leave unchaged
	  return
	fi
  done
  echo "$File"
}
export -f GetUniqueName

###############################################################################
# Rename and move ./File.ext to YYYYMMDD/YYYYMMDD-HHMMSS.ext
# With $1 as file name (jpg or avi or mp3 or anything that contains an embedded date such as an exif tag)
# We leave the file alone if not
# This is quite slow but it does the job !
# Note that there may be collisions if there's more than one file per second (fear not, you get prompted)
###############################################################################

function MoveUsingExif {
	if [ $# -ne 1 ]; then exit; fi

	# Extract date from file. This may not work on some types of files,
	# test it by running the egrep manually
	DateStr=$( strings -14 "$1" | egrep -m 1 "^20[0-9][0-9][ :/]?[0-9][0-9][ :/]?[0-9][0-9][ :/]?[0-9][0-9][ :/]?[0-9][0-9][ :/]?[0-9][0-9]" ) &&
	Date=$( echo "$DateStr" | sed -e "s/\(20[0-9][0-9]\)[ :/]*\([0-9][0-9]\)[ :/]*\([0-9][0-9]\).*/\1\2\3/" ) &&
	Time=$( echo "$DateStr" | sed -e "s/20[0-9][0-9][ :/]*[0-9][0-9][ :/]*[0-9][0-9][ :/]*\([0-9][0-9]\)[ :/]*\([0-9][0-9]\)[ :/]*\([0-9][0-9]\).*/\1\2\3/" ) &&
	# Convert the extension to lowercase
	Ext=$( echo "$1" | sed -e "s/.*\.\([^.]\)/\1/" | tr "[:upper:]" "[:lower:]" ) &&

	# Create directory if necessary
	mkdir -vp $Date &&
	
	Final=$( GetUniqueName "$Date/${Date}_$Time.$Ext" )
	
	# Move and rename file only if all of the above succeeded
	mv -iv "$1" "$Final"
}
export -f MoveUsingExif

###############################################################################
# Rename and move ./File.ext to YYYYMMDD/YYYYMMDD-HHMMSS.ext
# This is used when no exif tag is available. It uses the last-modified date
# so it's less accurate. The first method should be applied first
###############################################################################

function MoveUsingDate {
	if [ $# -ne 1 ]; then exit; fi

	# Use date of last modification
	DateStr=$( stat --format="%y" "$1" ) &&
	Date=$( echo "$DateStr" | sed -e "s/\(20[0-2][0-9]\)[ :/-]*\([0-9][0-9]\)[ :/-]*\([0-9][0-9]\).*/\1\2\3/" ) &&
	Time=$( echo "$DateStr" | sed -e "s/20[0-2][0-9][ :/-]*[0-9][0-9][ :/-]*[0-9][0-9][ :/]*\([0-9][0-9]\)[ :/]*\([0-9][0-9]\)[ :/]*\([0-9][0-9]\).*/\1\2\3/" ) &&
	# Convert the extension to lowercase
	Ext=$( echo "$1" | sed -e "s/.*\.\([^.]\)/\1/" | tr "[:upper:]" "[:lower:]" ) &&

	# Create directory if necessary
	mkdir -vp $Date &&
	
	Final=$( GetUniqueName "$Date/${Date}_$Time.$Ext" )
	
	# Move and rename file only if all of the above succeeded
	mv -iv "$1" "$Final"
}
export -f MoveUsingDate


###############################################################################
# Optional: Find files without added keywords in their names, left over from previous runs
# This is just a reminder to move your sorry ass and rename those files
###############################################################################

echo
echo "Files that you ough to rename to something more significant:"
find -iregex ".*/[0-9]*[-_][0-9]*\.[a-z2]*"


###############################################################################
# Optional: Find empty files
###############################################################################

echo
echo "Those files have size zero (corrupted filesystem ? Leftover temps ?):"
find -type f -size 0


###############################################################################
# Move files from digital camera(s) and/or memory card(s) to current directory
# Remember to reformat the card afterwards as files are sometimes not removed when using a cable
# (some cameras provide read-only access)
# Note: camera should be in 'Mass Storage' USB mode and connected (d'oh!)
###############################################################################

echo
echo "Moving every file from camera:"
mv -iv $PathCamera/* .

if [ $PathSecondary ]
then
	find $PathSecondary -iregex ".*\.\($iregex\)" -exec mv -iv "{}" . \;
fi

# Get rid of the possible rotation bits of jpg files
jhead -autorot *.jpg *.JPG *.jpeg *.JPEG


###############################################################################
# Rename according to exif date/time
# Original names are lost (but who cares)
###############################################################################

echo
echo "Renaming and moving files to final destination:"
# Add other file extensions here as needed
find -maxdepth 1 -iregex ".*\.\($iregex\)" -exec bash -c "MoveUsingExif '{}'" \;
find -maxdepth 1 -iregex ".*\.\($others\)" -exec bash -c "MoveUsingDate '{}'" \;

# Sometimes there are hiccups on Windows. This is just a harmless workaround for a cygwin bug. 
# mv -iv /*.dng .

echo
echo "Verify that the files have been properly moved and reformat your memory card."
echo "If there are jpg or raw files left in the present directory, run this script again."
echo "If there are some extensions that won't sort, add them to the 'others' variable."


# Files renamed in batch mode by SilkyPix can have the ending numerals removed with this:
# find -regex ".*[a-vxyzA-OQ-Z][0-9]+\..*" | sed -e "s%\./%%" -e "s/\(.*[a-vxyzA-OQ-Z]\)[0-9][0-9]*\(\..*\)/mv -i \0 \1\2/" > /tmp/$$
# /tmp/$$
